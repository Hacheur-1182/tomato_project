import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../helpers/endpoint.dart';

class AuthenticationProvider with ChangeNotifier {
  String? _token;
  DateTime? _expireTokenTime;
  String? _userName;

  bool get isAuthenticated {
    return token != null;
  }

  String? get token {
    if (_expireTokenTime != null &&
        _expireTokenTime!.isAfter(DateTime.now()) &&
        _token != null) {
      return _token;
    }
    return null;
  }

  Future<void> _signupRequest(
    String? name,
    String? email,
    String? password,
    String? number,
    String route,
  ) async {
    try {
      var url = Uri.parse(endPoint + route);
      final response = await http.post(
        url,
        headers: {
          "ContentType": "application/json",
          "Accept": "application/json",
        },
        body: {
          "name": name,
          "email": email,
          "password": password,
          "number": number
        },
      ).timeout(
        const Duration(seconds: 30),
        onTimeout: () =>
            throw const HttpException("Missing internet connection"),
      );
      final responseMessage = jsonDecode(response.body);

      if (responseMessage["error"] != null) {
        throw HttpException(responseMessage["error"]);
      }

      _userName = responseMessage["data"]["user"]["name"];
      _token = responseMessage["data"]["token"];
      _expireTokenTime = DateTime.now().add(
        Duration(
          seconds: responseMessage["data"]["token_life"],
        ),
      );
      if (kDebugMode) {
        // print(jsonDecode(response.body));
        // print(_expireTokenTime);
      }
      notifyListeners();
      final prefs = await SharedPreferences.getInstance();
      final user = jsonEncode({
        '_userName': _userName,
        'token': _token,
        'expiresIn': _expireTokenTime?.toIso8601String()
      });
      prefs.setString('user', user);
    } catch (error) {
      rethrow;
    }
  }

  Future<void> _loginRequest(
    String? email,
    String? password,
    String route,
  ) async {
    try {
      var url = Uri.parse(endPoint + route);
      final response = await http.post(
        url,
        headers: {
          "ContentType": "application/json",
          "Accept": "application/json",
        },
        body: jsonEncode({
          "email": email,
          "password": password,
        }),
      ).timeout(
        const Duration(seconds: 30),
        onTimeout: () =>
            throw const HttpException("Missing internet connection"),
      );
      final responseMessage = jsonDecode(response.body);
      // print(responseMessage["data"]);
      // print(responseMessage["data"]["user"]);
      // print(responseMessage["data"]["token"]);

      if (responseMessage["error"] != null) {
        throw HttpException(responseMessage["error"]);
      }

      _userName = responseMessage["data"]["user"]["name"];
      _token = responseMessage["data"]["token"];
      _expireTokenTime = DateTime.now().add(
        Duration(
          seconds: responseMessage["data"]["token_life"],
        ),
      );
      if (kDebugMode) {
        // print(jsonDecode(response.body));
        //  print(_expireTokenTime);
        //  print(isAuthenticated);
      }
      notifyListeners();
      final prefs = await SharedPreferences.getInstance();
      final user = jsonEncode({
        '_userName': _userName,
        'token': _token,
        'expiresIn': _expireTokenTime?.toIso8601String()
      });
      prefs.setString('user', user);
      print(prefs.getString("user"));
    } catch (error) {
      rethrow;
    }
  }

  void logout() async {
    _userName = null;
    _token = null;
    _expireTokenTime = null;
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey("user")) {
      return false;
    }
    final retrieveUser =
        jsonDecode(prefs.getString("user")!) as Map<String, dynamic>?;
    final expireIn = DateTime.parse("${retrieveUser!["expiresIn"]}");
    if (expireIn.isBefore(DateTime.now())) {
      return false;
    }
    _userName = retrieveUser['_userName'] as String?;
    _token = retrieveUser['expiresIn'] as String?;
    _expireTokenTime = expireIn;
    notifyListeners();
    return true;
  }

  Future<void> signup(
    String? name,
    String? email,
    String? password,
    String? number,
  ) async {
    return _signupRequest(name, email, password, number, "/signup");
  }

  Future<void> login(String? email, String? password) async {
    return _loginRequest(email, password, "/login");
  }
}
