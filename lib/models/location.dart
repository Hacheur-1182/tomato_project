class TrackLocation {
  final double distance;
  final Map<String, dynamic> myPosition;
  final Map<String, dynamic> trackModule;
  TrackLocation({
    required this.distance,
    required this.myPosition,
    required this.trackModule,
  });
}
