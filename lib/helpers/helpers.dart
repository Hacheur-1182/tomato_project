import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFF005C53);
const Color secondaryColor = Color(0xFFF2F2F2);
const Color cardColor = Color(0xFFD9D9D9);
