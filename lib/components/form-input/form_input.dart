import 'package:flutter/material.dart';

class FormInput extends StatelessWidget {
  const FormInput({
    Key? key,
    required this.label,
    required this.suffixIcon,
    required this.obscure,
    this.controller,
    this.saveEntry,
    this.validatorEntry,
  }) : super(key: key);

  final String label;
  final Icon suffixIcon;
  final bool obscure;
  final TextEditingController? controller;
  final void Function(String? value)? saveEntry;
  final String? Function(String? value)? validatorEntry;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: Colors.grey,
      obscureText: obscure,
      obscuringCharacter: "*",
      validator: validatorEntry,
      onSaved: saveEntry,
      decoration: InputDecoration(
        suffixIcon: suffixIcon,
        label: Text(
          label,
          style: const TextStyle(
            color: Colors.grey,
          ),
        ),
        focusColor: Colors.grey,
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.grey,
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.grey,
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
    );
  }
}
