import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:tomato/provider/authentication_provider.dart';
import 'package:tomato/routes/app_route.dart';


class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(10.0),
        child: FloatingActionButton(
          backgroundColor: Colors.white,
          onPressed: () {
            Navigator.pushNamed(context, AppRoutes.viewWevPage);
          },
          child: SvgPicture.asset(
            "assets/icon/player.svg",
            height: 18.0,
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height * 1,
        width: double.infinity,
        decoration: const BoxDecoration(
            // color: Colors.redAccent,
            image: DecorationImage(
          fit: BoxFit.cover,
          opacity: 0.15,
          image: AssetImage("assets/images/tomato.JPG"),
        )),
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10.0,
            top: 70,
          ),
          child: Column(
            children: [
              Row(
                children: [
                  const Text(
                    "Dashboard",
                    style: TextStyle(
                      fontSize: 25.0,
                    ),
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: () {
                      Provider.of<AuthenticationProvider>(context,
                              listen: false)
                          .logout();
                    },
                    icon: SvgPicture.asset("assets/icon/logout.svg"),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.12,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(
                          width: 1,
                          color: Colors.grey,
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 30.0),
                            child: Row(
                              children: [
                                const Text(
                                  "Refresh",
                                  style: TextStyle(
                                    fontSize: 18.0,
                                  ),
                                ),
                                const Spacer(),
                                IconButton(
                                  icon: const Icon(CupertinoIcons.refresh),
                                  onPressed: () {
                                    Navigator.pushReplacementNamed(
                                      context,
                                      AppRoutes.homePage,
                                    );
                                  },
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.all(20.0),
                        height: MediaQuery.of(context).size.height * 0.7,
                        width: double.infinity,
                        clipBehavior: Clip.antiAlias,
                        decoration: BoxDecoration(
                          color: const Color(0xFFF4F4F4).withOpacity(0.2),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: const Text(
                          "La noctuelle de la tomate ou chenille "
                          "de la tomate est l’un des "
                          "ravageurs les plus dévastateurs "
                          "dans les plantations agricoles. "
                          "Elle entre dans la catégorie des «nocturnes», "
                          "c’est à dire qu’elle est principalement active "
                          "la nuit, en particulier pour les insectes au stade adulte."
                          "La capacité d’adaptation de la chenille verte favorise "
                          "la présence de cette chenille "
                          "sur tout type de plantation de tomates,"
                          " que ce soit dans les plaines maraîchères "
                          "traditionnelles ou dans les cultures en hauteur"
                          "Les œufs sont déposés sur la plante. "
                          "Il en sortira des larves qui viendront grignoter "
                          "les feuilles des plants de tomate "
                          "et en manger une grande partie avant "
                          "de s’attaquer aux fruits.",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
