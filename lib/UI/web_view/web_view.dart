import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:tomato/helpers/endpoint.dart';
import 'package:tomato/helpers/helpers.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ViewWebPage extends StatefulWidget {
  const ViewWebPage({Key? key}) : super(key: key);

  @override
  State<ViewWebPage> createState() => _ViewWebPageState();
}

class _ViewWebPageState extends State<ViewWebPage> {
  final _key = UniqueKey();
  final Completer<WebViewController> _controller =
  Completer<WebViewController>();

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text("Dashboard"),
      ),
      body: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 1,
              decoration: BoxDecoration(
                color: primaryColor,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: WebView(
                key: _key,
                initialUrl: tomatoLink,
                javascriptMode: JavascriptMode.unrestricted,
                gestureNavigationEnabled: true,
                onWebViewCreated: (WebViewController webViewController) {
                  _controller.complete(webViewController);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
