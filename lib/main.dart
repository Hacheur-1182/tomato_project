import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:tomato/UI/web_view/web_view.dart';
import 'package:tomato/provider/authentication_provider.dart';
import 'package:tomato/routes/app_route.dart';

import 'UI/authentication/authentication.dart';
import 'UI/home/home.dart';
import 'connexionStatus/handling_connection.dart';
import 'helpers/custom_route_transition.dart';
import 'helpers/helpers.dart';

Future<void> main() async {
  // WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  // FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  // FlutterNativeSplash.remove();
  runApp(const TomatoApp());
}

class TomatoApp extends StatelessWidget {
  const TomatoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => AuthenticationProvider(),
        ),
      ],
      child: Consumer<AuthenticationProvider>(
        builder: (BuildContext context, authenticated, Widget? child) {
          return MaterialApp(
            theme: ThemeData(
              scaffoldBackgroundColor: Colors.white,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              textTheme: GoogleFonts.poppinsTextTheme(
                Theme.of(context).textTheme.copyWith(
                      bodyText1: GoogleFonts.poppins(
                        textStyle: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
              ),
              pageTransitionsTheme: PageTransitionsTheme(
                builders: {
                  TargetPlatform.android: CustomPageTransitionBuilder(),
                  TargetPlatform.iOS: CustomPageTransitionBuilder(),
                },
              ),
            ),
            home: authenticated.isAuthenticated
                ? const Home()
                : FutureBuilder(
                    initialData: authenticated.tryAutoLogin(),
                    future: authenticated.tryAutoLogin(),
                    builder: (BuildContext context,
                            AsyncSnapshot<dynamic> snapshot) =>
                        snapshot.connectionState == ConnectionState.waiting
                            ? const Scaffold(
                                body: Center(
                                  child: CircularProgressIndicator(
                                    color: primaryColor,
                                    backgroundColor: Colors.white,
                                  ),
                                ),
                              )
                            : const AuthenticationScreen(),
                  ),
            routes: {
              AppRoutes.homePage: (ctx) => const Home(),
              AppRoutes.connectionErrorPage: (ctx) =>
                  const HandlingConnectionState(),
              AppRoutes.viewWevPage: (ctx) => const ViewWebPage(),
            },
          );
        },
      ),
    );
  }
}
